import React, {Component} from 'react';
import {AsyncStorage, FlatList, RefreshControl, StyleSheet, Text, View} from 'react-native';
import {getTeams} from '../services';
import {Provider as PaperProvider} from 'react-native-paper'
import ListItem from '../components/ListItem';

export default class HomeScreen extends Component {
    static navigationOptions = {
        title: 'Campeonato Pernambucano'
    };

    state = {
        teams: [],
        isLoading: false,
        favTeam: '',
        isHiddenFav: true,
    };

    async componentDidMount() {
        this.loadData();
    }

    loadData = async () => {
        const response = await getTeams();
        this.setState({teams: response.data});

        const favTeam = JSON.parse(await AsyncStorage.getItem('favTeam'));
        if (JSON.stringify(favTeam) !== "{}") {
            this.setState({favTeam: favTeam.name, isHiddenFav: false});
        }
    };

    showTeamDetails = (team) => {
        this.props.navigation.navigate('TeamDetails', {team});
    };

    render() {
        return (
            <PaperProvider>
                <FlatList
                    data={this.state.teams}
                    renderItem={({item}) =>
                        <ListItem
                            item={item}
                            onTeamPress={() => this.showTeamDetails(item)}
                        />}
                    keyExtractor={(item) => item.id.toString()}
                    ItemSeparatorComponent={() => <View style={{height: 4, backgroundColor: '#fff'}}/>}
                    refreshing={this.state.isLoading}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this.loadData}/>
                    }
                />
                {this.state.isHiddenFav ? null :
                    <View style={styles.containerFav}>
                        <Text style={{fontSize: 13}}>Favorito: {this.state.favTeam}</Text>
                    </View>
                }
            </PaperProvider>
        );
    }
}

const styles = StyleSheet.create({
    containerFav: {
        width: '100%',
        backgroundColor: '#fff',
        alignItems: 'center',
        height: 30,
        justifyContent: 'center',
    },
});