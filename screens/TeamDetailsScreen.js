import React, {Component} from 'react';
import {AsyncStorage, Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {Provider as PaperProvider} from 'react-native-paper'
import FavButton from '../components/favButton';

export default class TeamDetailsScreen extends Component {
    static navigationOptions = ({navigation}) => ({
        title: navigation.getParam('team').name,
        headerTintColor: '#000',
    });

    state = {
        favTeam: {},
        isFavTeam: false,
        icon: 'star',
    };

    async componentDidMount() {
        const favTeam = await AsyncStorage.getItem('favTeam');
        const team = this.props.navigation.getParam('team');
        let icon = 'star';
        let favt = false;
        if (/*favTeam !== null &&*/ favTeam.id === team.id) {
            icon = 'star'; //deve ser star pintada!
            favt = true;
            console.log("star-colored");
        }
        this.setState({favTeam: JSON.parse(favTeam) || {}, isFavTeam: favt, icon: icon});
    };

    checkAddOrDelete = (team) => {
        if (this.state.favTeam.id === team.id) {
            this.deleteFavTeam();
            console.log('del ' + team.name);
        } else {
            this.addFavTeam(team);
            console.log('add ' + team.name);
        }
    };

    addFavTeam = (team) => {
        this.setState({favTeam: team, isFavTeam: true, icon: 'star'}, this.saveFavTeam);
        alert('Time adicionado com sucesso!')
    };

    deleteFavTeam = () => {
        this.setState({favTeam: {}}, this.saveFavTeam);
        alert('Time removido com sucesso!')
    };

    saveFavTeam = () => {
        AsyncStorage.setItem('favTeam', JSON.stringify(this.state.favTeam));
    };

    render() {
        const team = this.props.navigation.getParam('team');

        return (
            <PaperProvider>
                <ScrollView>
                    <View style={styles.contentHeader}>
                        <Image
                            style={styles.teamImage}
                            source={{uri: `http://egcservices.com.br/webservices/appreact/images/${team.image}`}}/>
                        <Text style={{fontSize: 24, marginHorizontal: 15}}>
                            {team.name}
                        </Text>
                        <Text style={{fontSize: 18}}>
                            ({team.trainer})
                        </Text>
                    </View>
                </ScrollView>
                <FavButton style={styles.fab} icon={this.state.icon} onPress={() => this.checkAddOrDelete(team)}/>
            </PaperProvider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentHeader: {
        width: '100%',
        alignItems: 'center',
        marginTop: 20,
    },
    fab: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        backgroundColor: '#6878ff'
    },
    teamImage: {
        width: 92,
        height: 92,
        marginHorizontal: 15,
    },
});
