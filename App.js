import React from 'react';
import {createStackNavigator} from 'react-navigation';
import HomeScreen from './screens/HomeScreen';
import TeamDetailsScreen from './screens/TeamDetailsScreen';

const App = createStackNavigator({
    Home: {
        screen: HomeScreen
    },
    TeamDetails: {
        screen: TeamDetailsScreen
    }
});

export default App;
