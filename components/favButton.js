import React from 'react';
import {FAB} from 'react-native-paper';

const FavButton = ({style, icon, onPress}) =>
    <FAB style={style} icon={icon} onPress={onPress}/>;
export default FavButton;