import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

export default class ListItem extends Component {

    render() {
        const {item, onTeamPress} = this.props;
        return (
            <TouchableOpacity style={styles.row} onPress={onTeamPress}>
                <Image
                    style={styles.teamImage}
                    source={{uri: `http://egcservices.com.br/webservices/appreact/images/${item.image}`}}/>
                <View>
                    <Text>{item.name}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
    },
    teamImage: {
        width: 32,
        height: 32,
        marginHorizontal: 10,
    },
});