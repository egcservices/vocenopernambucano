export const getTeams = async () => {
    const response = await fetch('http://egcservices.com.br/webservices/appreact/teams.json');
    return await response.json();
};